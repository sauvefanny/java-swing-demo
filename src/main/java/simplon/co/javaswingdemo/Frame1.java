package simplon.co.javaswingdemo;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class Frame1 extends JFrame {

    JFrame f = new JFrame("Ma Pizzeria");

    Frame1(){

    JLabel l1, l2, l3, l4;
    l1=new JLabel("Caisse automatique");
    l1.setBounds(130,20,200,30);
    l2=new JLabel("Nombre de pizzas :");
    l2.setBounds(50,100,150,30);l3=new JLabel("Résultat :");
    l3.setBounds(50,150,200,30);l4=new JLabel("");
    l4.setBounds(200,150,100,30);

    
    Border border = BorderFactory.createLineBorder(Color.BLUE, 2);
    l3.setBorder(border);

    JTextField t1;
    t1= new JTextField();
    t1.setBounds(200,100,70,30);

    f.add(l1);
    f.add(l2);
    f.add(l3);
    f.add(l4);
    f.add(t1);

    JButton result = new JButton("Calculer le total");// creating instance of JButton
    result.setBounds(100,350,200,40);// x axis, y axis, width, height
    f.add(result);// adding button in JFrame
    
    JCheckBox cheeseAdd = new JCheckBox("supplément fromage");
    cheeseAdd.setBounds(100,400,300, 40);
    f.add(cheeseAdd);


    result.addActionListener(new ActionListener(){

    public void actionPerformed(ActionEvent evt) {
        double a = Integer.parseInt(t1.getText());
        double sum = 0;
        sum = (a * Pizza.prices);

        if (cheeseAdd.isSelected()){
             sum += CheeseAdd.pricesAdd;
               }
        l4.setText(String.valueOf(sum));
    }
});

f.setSize(400,500);// 400 width and 500 height
f.setLayout(null);// using no layout managers
f.setVisible(true);// making the frame visible
f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}
}
